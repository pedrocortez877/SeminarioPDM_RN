import React from 'react';
import {  
  View,
  Text,
  StyleSheet,
  StatusBar,
  Image,
  ScrollView
} from 'react-native';

import {useRoute} from '@react-navigation/core';

export function SerieDetails() {
  const route = useRoute();
  const { data } = route.params;
  return(
    <View style={styles.container}> 
      <StatusBar barStyle='light-content' translucent backgroundColor="transparent"/>
      <Image source={{uri: data.avatar}} style={styles.image} resizeMode="cover"/>

      <ScrollView style={styles.scrollContainer}>

        <View style={styles.nameClassArea}>
          <Text style={styles.name}>{data.name}</Text>
          <View 
            style={[styles.class, 
            data.class == 18 && {backgroundColor: '#000'},
            data.class == 16 && {backgroundColor: '#FF0000'},
            data.class == 12 && {backgroundColor: '#f8c411'},]}
          >
            <Text style={styles.textClass}>{data.class}</Text>
          </View>
        </View>
        
        <View style={styles.aboutArea}>
          <Text style={styles.about}>{data.about}</Text>
        </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#d9d9d9'
  },
  image: {
    width: '100%',
    height: 240,
  },
  scrollContainer: {
    flex: 1,
    marginHorizontal: 20,
    padding: 20,
    backgroundColor: '#FFF',
    borderRadius: 30,
    marginTop: -40,
    marginBottom: 30
  },
  nameClassArea:{
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  name: {
    fontFamily: 'Jost_600SemiBold',
    fontSize: 24,
    color: '#52665A'
  },
  class: {
    height: 40,
    width: 40,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'flex-end'
  },
  textClass: {
    fontWeight: 'bold',
    color: '#FFF',
    fontSize: 18,
  },
  aboutArea: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 25,
    padding: 5
  },
  about: {  
    fontFamily: 'Jost_400Regular',
    fontSize: 16,
    textAlign: 'justify',
    color: '#52665A'
  }

});