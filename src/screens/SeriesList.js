import React, {useState, useEffect} from 'react';
import {  
  View,
  ScrollView,
  StyleSheet,
  Image,
  StatusBar,
  TextInput,
  Text
} from 'react-native';
import Swiper from 'react-native-swiper';
import { MaterialIcons } from '@expo/vector-icons';

import Card from '../components/Card';

import api from '../services/api.js';

export function SeriesList() {
  const [series, setSeries] = useState([]);
  const [seriesFiltered, setSeriesFiltered] = useState([]);
  const [statusBarColor, setStatusBarColor] = useState('light-content');

  useEffect(() => {
    async function searchSeries(){
      const { data } = await api.get('series');
      setSeries(data);
      setSeriesFiltered(data);
    }
  
    searchSeries();
  }, []);

  function handleScroll(event){
    let scrollDistance = event.nativeEvent.contentOffset.y;
    if(scrollDistance >= 224){
      setStatusBarColor('dark-content');
    }else{
      setStatusBarColor('light-content');
    }
  }

  function handleChangeText(text){
    setSeriesFiltered(
      series.filter((data) => (
        data.name.toUpperCase().includes(text.toUpperCase())
      ))
    );
  }

  return(
    <ScrollView style={styles.container} onScroll={handleScroll}>
      <StatusBar 
        barStyle={statusBarColor} 
        translucent 
        backgroundColor="transparent"
      />
      <View style={styles.swiperArea}>
        <Swiper
          style={{height: 250}}
          autoplay={true}
          horizontal
        >
          {
            series.map((item, key) => (
              <View style={styles.swipeItem} key={key}>
                <Image 
                  style={styles.swipeImage} 
                  source={{uri: item.avatar}} 
                  resizeMode="cover"
                />
              </View>
            ))
          }
        </Swiper>
      </View> 

      <Text style={styles.searchDescription}>
          Pesquise sua série favorita:
      </Text>

      <View style={styles.searchArea}>
          <MaterialIcons 
            name="search" 
            size={25} 
            color="#bababa" 
            style={styles.searchIcon}
          />
          <TextInput 
            style={styles.searchInput} 
            placeholder="Digite o nome de uma série" 
            onChangeText={handleChangeText}
          />
      </View>

      <View style={styles.listArea}>
          {seriesFiltered.map((item, key) => (
            <Card key={key} data={item}/>
          ))}
      </View>

    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ebebeb'
  },
  swiperArea: {
    flex: 1,
    flexDirection: 'column'
  },
  swipeItem: {
    flex: 1,
    height: 250
  },
  swipeImage: {
    width: '100%',
    height: 240
  },
  searchDescription: {
    paddingHorizontal: 20,
    fontFamily: 'Jost_600SemiBold',
    fontSize: 20,
    color: '#52665A',
  },
  searchArea:{
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'flex-start',
    marginTop: 20
  },
  searchInput: {
    backgroundColor: '#d6d6d6',
    width: 260,
    height: 45,
    paddingLeft: 0,
    borderRadius: 15,
    textAlign: 'center'
  },
  searchIcon: {
    marginRight: -35,
    zIndex: 99,
    marginTop: 10
  },
  listArea: {
    flex: 1,
    justifyContent: 'flex-start',
    marginTop: 30
  }
});