import React from 'react';
import { View, StyleSheet, TouchableOpacity, Image, Text }from 'react-native';
import {useNavigation}  from '@react-navigation/core';

export default function Card({data}) {
  const navigation = useNavigation();
  let description = data.about;

  if(description.length > 60){
    description = description.substring(0, 60) + '...';
  }

  function handleSelectSerie(){
    navigation.navigate('SerieDetails', {data});
  }

  return (
    <TouchableOpacity style={styles.container} onPress={handleSelectSerie}>
      <Image style={styles.image} source={{uri: data.avatar}}/>

      <View style={styles.infoArea}>
        <Text style={styles.name}>{data.name}</Text>

        <Text style={styles.about}>
          {description}
        </Text>

        <View 
          style={[styles.class, 
          data.class == 18 && {backgroundColor: '#000'},
          data.class == 16 && {backgroundColor: '#FF0000'},
          data.class == 12 && {backgroundColor: '#f8c411'},]}
        >
          <Text style={styles.textClass}>{data.class}</Text>
        </View>
      </View> 
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF',
    marginBottom: 20,
    borderRadius: 20,
    padding: 15,
    flexDirection: 'row'
  },
  image: {
    width: 90,
    height: 90,
    borderRadius: 15
  },
  infoArea: {
    flex: 1,
    marginLeft: 20,
    justifyContent: 'space-between',
  },
  name: {
    fontFamily: 'Jost_600SemiBold',
    fontSize: 24,
    color: '#52665A'
  },
  class: {
    height: 40,
    width: 40,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'flex-end'
  },
  textClass: {
    fontWeight: 'bold',
    color: '#FFF',
    fontSize: 18,
  },
  about: {
    fontSize: 14,
    textAlign: 'justify',
    color: '#52665A'
  }
});