import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import { SeriesList } from '../screens/SeriesList';
import { SerieDetails } from '../screens/SerieDetails';

const stackRoutes = createStackNavigator();

export default function MainStack(){
  return(
    <stackRoutes.Navigator
      headerMode="none"
      screenOptions={{
        cardStyle: {
          backgroundColor: "#FFF"
        },
      }}
      initialRouteName="SeriesList"
    >
        <stackRoutes.Screen name="SeriesList" component={SeriesList}/>
        <stackRoutes.Screen name="SerieDetails" component={SerieDetails}/>
    </stackRoutes.Navigator>
  );
}