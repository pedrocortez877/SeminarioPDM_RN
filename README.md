<h1 align="center">Seminário PDM</h1>

## Descrição do projeto

<p>Este projeto foi desenvolvido com intuito de demonstrar a tecnologia React Native, onde foi criado um app
de listagem de series, demonstrando componentização, rotas etc.</p>

### Status do projeto

<h4 align="center"> 
	✅ Projeto Finalizado ✅
</h4>

### Pré-requisítos & instalação

Antes de começar, você vai precisar ter instalado em sua máquina as seguintes ferramentas:
[Git](https://git-scm.com), [Node.js](https://nodejs.org/en/), [Expo](https://expo.io)
Além disto é bom ter um editor para trabalhar com o código como [VSCode](https://code.visualstudio.com/)

### 🎲 Rodando a aplicação

```bash
# Clone este repositório
$ git clone https://gitlab.com/pedrocortez877/SeminarioPDM_RN.git

# Acesse a pasta do projeto no terminal/cmd
$ cd SeminarioPDM_RN

# Instale as dependências 
$ cd web
$ npm install  - Caso use npm
$ yarn - Caso use yarn

# Execute o servidor em modo de desenvolvimento
$ json-server ./src/services/server.json --host <Seu endereço Ip> --port 3000
Atenção: Se o seu endereço IP for diferente do que esta inserido no: src/services/api.js insira-o no BASE_URL

# O servidor inciará na porta:3000 - acesse <http://<EndereçoIp>:3000>

# Execute o projeto
$ expo start 
Após isso, inicie a aplicação escaneando o QR code (Para isso você ira precisar do app do expo no celular)
ou execute o projeto no emulador
```

### 🛠 Tecnologias

As seguintes ferramentas foram usadas na construção do projeto:

- [React Native](https://reactnative.dev)
- [Expo](https://expo.io)
- [NodeJS](https://nodejs.org/pt-br/)

### Autores
---

<a href="#">
 <img style="border-radius: 50%;" src="https://github.com/pedrocortez877.png" width="100px;" alt=""/>
 <sub><b>Pedro Cortez</b></sub></a>
</a>

<a href="#">
 <img style="border-radius: 50%;" src="https://avatars.githubusercontent.com/u/69906538?v=4" width="100px;" alt=""/>
 <sub><b>Gustavo Bessa</b></sub>
</a>

<a href="#">
 <img style="border-radius: 50%;" src="https://cactusthemes.com/blog/wp-content/uploads/2018/01/tt_avatar_small.jpg" width="100px;" alt=""/>
 <sub><b>Rafael Assunção</b></sub>
</a>
