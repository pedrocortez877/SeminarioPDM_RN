import React from 'react';
import { NavigationContainer } from '@react-navigation/native';

import { useFonts, Jost_600SemiBold, Jost_400Regular } from '@expo-google-fonts/jost'

import MainStack from './src/stacks/MainStack';

export default function App() {
  const [fontsLoaded] = useFonts({
    Jost_600SemiBold,
    Jost_400Regular
  });

  if(!fontsLoaded){
    return null
  }

  return (
    <NavigationContainer>
      <MainStack />
    </NavigationContainer>
  );
}

